// http://stackoverflow.com/questions/3437786/get-the-size-of-the-screen-current-web-page-and-browser-window

function window_width () {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0];
    return w.innerWidth || e.clientWidth || g.clientWidth;
}

function window_height () {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0];
    return w.innerHeight|| e.clientHeight|| g.clientHeight;
}

