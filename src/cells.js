import "extend";
import "window";

splitscreen.cells = ss_cells;

function ss_cells (opts) {
	opts = extend({}, {
		place: function (x, y, w, h) {
			this.style.left = x+"px";
			this.style.top = y+"px";
			this.style.width = w+"px";
			this.style.height = h+"px";
		}
	}, opts);

	var that = {},
		occupied_cells = [],
		gridsize = 0,
		grid = [];

	function resize_grid () {
		for (var y=0; y<gridsize; y++) {
			var row = grid[y];
			if (row === undefined) { row = grid[y] = []; }
			for (var x=0; x<gridsize; x++) {
				var cell = row[x];
				if (cell === undefined) { cell = row[x] = {x:x, y:y, elt: null} }
			}
		}
	}

	function free_cell () {
		for (var r=0; r<gridsize; r++) {
			var row = grid[r];
			for (var c=0; c<gridsize; c++) {
				var cell = row[c];
				if (cell.elt === null) { return cell }
			}
		}
		// expand gridsize
		gridsize += 1;
		console.log("expanded gridsize", gridsize);
		resize_grid();
		return free_cell();		
	}

	that.add = function (elt) {
		var cell = free_cell();
		cell.elt = elt;
		occupied_cells.push(cell);
		return that;
	}

	that.remove = function (elt) {
		for (var i=0; i<occupied_cells.length; i++) {
			var cell = occupied_cells[i];
			if (cell.elt == elt) {
				cell.elt = null;
				occupied_cells.splice(i, 1);
				return that;
			}
		}
		return that;
	}

	function layout () {
		var ww = window_width(),
			wh = window_height(),
			// gridsize = Math.ceil(Math.sqrt(cells.length)),
			cellwidth = ww/gridsize,
			cellheight = wh/gridsize;
		for (var r=0; r<gridsize; r++) {
			var row = grid[r];
			for (var c=0; c<gridsize; c++) {
				var cell = row[c];
				if (cell.elt !== null) {
					// console.log("placing", cell);
					opts.place.call(cell.elt, (cell.x*cellwidth), (cell.y*cellheight), cellwidth, cellheight)
				}
			}
		}
		return that;
	}
	that.layout = layout;

	function get () {
		if (arguments.length == 0) {
			// get() returns list of elements
			return occupied_cells.map(function (x) { return x.elt });
		} else {
			// get (i) return item [i], neg indices are from end
			var i = arguments[0];
			if (i >= 0) {
				// normal index lookup
				return occupied_cells[i].elt;
			} else {
				// support negative (from end) indexes
				return occupied_cells[occupied_cells.length+i].elt;
			}
		}
	}
	that.get = get;

	return that;
}