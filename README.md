splitscreen.js
==============

Javascript to display links in a grid of frames.

See [demo](splitscreen.html).

Features
* Ability to group frames in various hierarchies
* Support for STACKS + GRID displays.
* Support for gestures to navigate STACKS + GRIDS.
* Support for various levels of zoom.
* Support for deferred load panes (to be loaded only when navigated to), and "posters" (image placeholders).

