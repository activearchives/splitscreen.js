!function() {
  var splitscreen = {
    version: "1.0"
  };
  function extend() {
    for (var i = 1; i < arguments.length; i++) for (var key in arguments[i]) if (arguments[i].hasOwnProperty(key)) arguments[0][key] = arguments[i][key];
    return arguments[0];
  }
  function window_width() {
    var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName("body")[0];
    return w.innerWidth || e.clientWidth || g.clientWidth;
  }
  function window_height() {
    var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName("body")[0];
    return w.innerHeight || e.clientHeight || g.clientHeight;
  }
  splitscreen.cells = ss_cells;
  function ss_cells(opts) {
    opts = extend({}, {
      place: function(x, y, w, h) {
        this.style.left = x + "px";
        this.style.top = y + "px";
        this.style.width = w + "px";
        this.style.height = h + "px";
      }
    }, opts);
    var that = {}, occupied_cells = [], gridsize = 0, grid = [];
    function resize_grid() {
      for (var y = 0; y < gridsize; y++) {
        var row = grid[y];
        if (row === undefined) {
          row = grid[y] = [];
        }
        for (var x = 0; x < gridsize; x++) {
          var cell = row[x];
          if (cell === undefined) {
            cell = row[x] = {
              x: x,
              y: y,
              elt: null
            };
          }
        }
      }
    }
    function free_cell() {
      for (var r = 0; r < gridsize; r++) {
        var row = grid[r];
        for (var c = 0; c < gridsize; c++) {
          var cell = row[c];
          if (cell.elt === null) {
            return cell;
          }
        }
      }
      gridsize += 1;
      console.log("expanded gridsize", gridsize);
      resize_grid();
      return free_cell();
    }
    that.add = function(elt) {
      var cell = free_cell();
      cell.elt = elt;
      occupied_cells.push(cell);
      return that;
    };
    that.remove = function(elt) {
      for (var i = 0; i < occupied_cells.length; i++) {
        var cell = occupied_cells[i];
        if (cell.elt == elt) {
          cell.elt = null;
          occupied_cells.splice(i, 1);
          return that;
        }
      }
      return that;
    };
    function layout() {
      var ww = window_width(), wh = window_height(), cellwidth = ww / gridsize, cellheight = wh / gridsize;
      for (var r = 0; r < gridsize; r++) {
        var row = grid[r];
        for (var c = 0; c < gridsize; c++) {
          var cell = row[c];
          if (cell.elt !== null) {
            opts.place.call(cell.elt, cell.x * cellwidth, cell.y * cellheight, cellwidth, cellheight);
          }
        }
      }
      return that;
    }
    that.layout = layout;
    function get() {
      console.log("arguments", arguments);
      if (arguments.length == 0) {
        return occupied_cells.map(function(x) {
          return x.elt;
        });
      } else {
        var i = arguments[0];
        if (i < 0) {
          return occupied_cells[occupied_cells.length + i].elt;
        } else {
          return occupied_cells[i].elt;
        }
      }
    }
    that.get = get;
    return that;
  }
  if (typeof define === "function" && define.amd) define(splitscreen); else if (typeof module === "object" && module.exports) module.exports = splitscreen;
  this.splitscreen = splitscreen;
}();