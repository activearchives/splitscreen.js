GENERATED_FILES = \
	splitscreen.html \
	splitscreen.js \
	splitscreen.min.js

all: $(GENERATED_FILES)

%.html: %.md
	pandoc --from markdown --to html --standalone --css styles.css $< -o $@

splitscreen.js: $(shell node_modules/.bin/smash --ignore-missing --list src/splitscreen.js)
	@rm -f $@
	node_modules/.bin/smash src/splitscreen.js | node_modules/.bin/uglifyjs - -b indent-level=2 -o $@
	@chmod a-w $@

splitscreen.min.js: splitscreen.js bin/uglify
	@rm -f $@
	bin/uglify $< > $@

clean:
	rm -f -- $(GENERATED_FILES)
