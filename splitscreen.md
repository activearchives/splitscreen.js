splitscreen.js
========================

* [State of Libre Graphics (ogv)](http://video.constantvzw.org/LGM15/day-01/01-state_of_libre_graphics.ogv)
* [State of Libre Graphics (pdf)](http://video.constantvzw.org/LGM15/day-01/01-state_of_libre_graphics.pdf)
* [Pathum Egodawatta Jumping (ogv)](http://video.constantvzw.org/LGM15/day-01/06-Pathum_Egodawatta-Jumping.ogv)
* [Matt Lee (ogv)](http://video.constantvzw.org/LGM15/day-01/03-Matt_Lee-List_CC.ogv)
* [Matt Lee (pdf)](http://video.constantvzw.org/LGM15/day-01/03-Matt_Lee-List_CC.pdf)
* [Jon Norby imgflo (ogv)](http://video.constantvzw.org/LGM15/day-01/04-Jon_Nordby-imgflo.ogv)
* [Jon Norby (pdf)](http://video.constantvzw.org/LGM15/day-01/04-Jon-Nordby-Imgflo.pdf)

<script src="splitscreen.js"></script>
<script>
splitscreen();
</script>
